/**
 *
 * ### Testing Basics ###
 *
 * Your code should have tests. It doesn't matter if your the only one on the project. Tests help ensure that the code can run, and is probably error lite.
 *
 * - Tests prevent Errors
 * - Tests ensure that if someone else alters the code, they know that they might have broken something.
 * - Tests are not a magic bullet to good code, but they help a lot.
 *
 * For this demo we will be using Jest. We could use Mocha or some others but the concepts are the same. They all require up front configuration. They all mock external modules. They all can do coverage. They all test code.
 *
 */

/**
 *
 * #### Setup ####
 *
 * Jest can run tests out of the box with javascript. If you use Typescript, which is just syntactic sugar on javascript, you will need some extra stuff.
 * Ensure your package.json has "babel-jest", "jest", "ts-jest" in its dev dependencies.
 */

// test a file
// test one test
// Test setup
// Coverage setup
// Test Methodology
// spy a function
// Mock a lib
// Spy a function on a mocked lib
