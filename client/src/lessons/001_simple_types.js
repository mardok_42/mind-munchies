/**
 * ### Javascript Simple Types ###
 *
 * For a more in depth look at types visit here: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures
 *
 * There are 6 primitive data types in Javascript but for 99% of the time you will only run across 4 of them.
 * - undefined (Nothing is set)
 * - Boolean (true or false)
 * - Number (its a number)
 * - BigInt (it can be a bigger number)
 * - String ('usually thought of as text')
 * - Symbol (unique unchangeable value often used as a key on objects)
 *
 * Besides the primitive data types there are two other types
 * - Object (this is a container that holds key value pairs sometimes called a library)
 * - function (sometimes called a method, or closure) - This is really an object too. But we will go into that later.
 */

// Usage:
// undefined type - This is what a variable is, if it has not been set.
let val_undefined;
const isUndefined = typeof val_undefined === "undefined"; // true because it has never been set
console.log("isUndefined: ", isUndefined); // true

// Boolean type - It can be either true, or false
const val_boolean = false;
const isBoolean = typeof val_boolean === "boolean"; // booleans can only have a value of true or false
console.log("isBoolean: ", isBoolean); // true because it is either true or false

// Number type - Double-precision floating-point format - https://en.wikipedia.org/wiki/Double-precision_floating-point_format, they can get pretty big, but just in case you need bigger, use BigInt
const val_number = 4;
const isNumber = typeof val_number === "number"; // a number can be any non string number. "4" !== 4. "4" is a string. 4 is a number

// BigInt - because sometimes you need really big numbers. You can use them with the basic math operators.
// You can declare them in two ways.
const myReallyBigNumber = BigInt(9007199254740991456789876); // You can call BigInt
const myOtherReallyBigNumber = 9007199254740991456789876n; // or simply add an "n" at the end

const plus1 = myOtherReallyBigNumber + 1n;
console.log("plus1: ", plus1); // 9007199254740991456789877n

// String
const val_string = "four";
const isString = typeof val_string === "string"; // a string is usually thought of as an array of characters.

// Symbol - unique and immutable primitive sometimes used as a key on objects.
const sym1 = Symbol("12345");
const sym2 = Symbol("12345");

// This condition will always return 'true' since the types 'typeof sym1' and 'typeof sym2' have no overlap.
// They refer to memory locations not the string that has been created with them.
const isNotTheSame = sym1 !== sym2;
console.log("isNotTheSame: ", isNotTheSame);
// -------------------------------------------------------------------------------------

// Object - Objects are containers or structures that hold types as their values. They are groups of key value pairs. The key can be any string, number or Symbol.
let obj = {}; // Curly Braces define the scope of the object.

// You can use an object in several ways.
// All these do the same thing

obj = { thing1: "foo" };

obj.thing1 = "foo";

obj["thing1"] = "foo";

const key1 = "thing1";
obj[key1] = "foo";

Object.assign(obj, { thing1: "foo" });
obj = { ...obj, thing1: "foo" };

// You can get the value out in several ways

let { thing1 } = obj;

thing1 = obj.thing1;

thing1 = obj["thing1"];

const key = "thing1";
thing1 = obj[key];

// Null - null is a special object type, like undefined no value is set, but it means it was explicitly set to nothing.
const val2 = null;
typeof val2 === "object"; // true

// Arrays - Arrays are objects. But they are objects that have limited their keys to numbers, and added some cool functions onto themselves.

const val = [];
val[2] = "foo";
typeof val === "object"; // true
