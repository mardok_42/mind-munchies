import { frozenTest, sealedTest } from "./006_object_descriptors";

describe("fun with objects", () => {
  it("tests sealed", () => {
    sealedTest();
  });

  it("tests frozen", () => {
    frozenTest();
  });
});
