### Web Application Development with Node JS

A Web Application is a website that goes beyond just displaying stuff. It allows the user to interact with it, and with other users to do stuff.

When I say stuff it is a very broad field.
It could be:

- Merchant Services
- Chat rooms
- Video Streaming
- Photo Editing
- Music Streaming
- Video Conferencing
- Games in all their varieties
- Mobile Applications
- Virtual Classrooms
- It is endless in possibilities

The core of it is, you have a client ( PC or Mobile Device ), and you have a server (Some external source of data and logic on the web), and they can exchange information.

Node JS is a server side Javascript build on C++. Using Node allows you to have your client and server written in the same language so you and your team can be less specialized. Sure knowing Java, PHP, or .NET is useful, but the project can get going faster if the team is working in the same language. Node JS allows that.

#### Node JS

For this project to run you should have Node installed on your machine. Visit this site if you do not.

- https://nodejs.org/en/

#### Node Version Manager

You might also want "Node Version Manager" if you are running on a Mac or Linux. This allows you to easily switch between node versions if you want to upgrade your node, or build on a specific version that the server will be running on.

- https://github.com/nvm-sh/nvm

#### Node Package Manager

You should get familiar with Node Package Manager (NPM). It is the primary source of node packages for projects.

- https://www.npmjs.com/

#### Yarn

This is a useful wrapper on NPM. It can be very helpful to smooth out some of the rough edges that NPM has. If you are going to use yarn just use yarn not npm, and vise versa. Mixing package management can cause unexpected issues. If you are switching package managers, remove the .lock file and node_modules. Then just use the new package manager. This also works if the project gets into a bad state. Removeing the lock file and node_modules and reinstalling can fix lots of issues.

- https://classic.yarnpkg.com/en/

#### The package.json

The package.json file at the root of the project has all the things that make the work. The "dependencies" and "devDependencies" objects reference all the modules it takes to run the project. The "scripts" have an object that has commands that can be run from the command line. It is the heart of the project.

A script command is run in this format:

# npm run command

OR

# yarn command

So if your package.json had this script in it:

"scripts": {
"doStuff": "node ./scripts/doStuff.js"
}

You could run it with this command:

# npm run doStuff

OR

# yarn doStuff

### Installing Modules

To add or install a module use this command.

# npm install packageName

OR

# yarn add packageName

To add it at a specific version add @ and the version number

# npm install packageName@2.34.4

OR

# yarn add packageName@2.34.4

### Installing a dev dependency

Dev dependencies should not be packaged at compile time to help reduce final file size. Any module in the devDependencies will not be compiled to the final project.
To add a module to the dev dependencies

# npm install packageName --save-dev

OR

# yarn add packageName --dev

### Removing Packages

To remove a package

# npm uninstall packageName

OR

# yarn remove packageName

### Install Everything

# npm install

OR

# yarn

#### Organization

A node project is typically started with a "start" command. Which should start a server. If the project is the front end you will be able to see it from your browser. Typically at a localhost address:
Example: http://localhost:3000
For the server side you may want to run Postman to help test your server locally
https://www.postman.com/
You will also be hitting a localhost address, but it may not be a method that is accessible with a browser. We will go over REST Request verbs later such as "GET", "POST", "PUT", "PATCH", and "DELETE".
