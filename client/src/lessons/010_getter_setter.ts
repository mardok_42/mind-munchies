/**
 * ### Object getters and setters
 *
 * getters and setters are special functions on an Object. They
 *
 */
let value = 0;

export const getterSetterObject = {
  get someValue() {
    console.log("getter called");
    return value;
  },
  set someValue(val: number) {
    // Getters and setters can have the same name.
    console.log("setter called");
    value = val * 2;
  },
  get getValue() {
    // Or you can have different names
    console.log("getter called too");
    return value;
  },
  set setValue(val: number) {
    console.log("setter called too");
    value = val;
    // The value of a setter that has no getter by the same name will be undefined
  }
};
