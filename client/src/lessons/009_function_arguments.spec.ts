import { argumentsLength, argumentsLengthToo, thirdArguement, withDefault } from "./009_function_arguments";

describe("function arguments", () => {
  it("gets the arguments length", () => {
    // @ts-ignore
    const len = argumentsLength(1, 2, 3, 4);

    expect(len).toBe(4);
  });

  it("gets the arguments length too", () => {
    // @ts-ignore
    const len = argumentsLengthToo(1, 2, 3, 4);

    expect(len).toBe(4);
  });

  it("gets the third arguments", () => {
    // @ts-ignore
    const len = thirdArguement(1, 2, 3, 4);

    expect(len).toBe(3);
  });

  it("gets a default", () => {
    const res = withDefault();

    expect(res).toBe("junk");
  });

  it("gets a value", () => {
    const expected = "more stuff";
    const res = withDefault(expected);

    expect(res).toBe(expected);
  });
});
