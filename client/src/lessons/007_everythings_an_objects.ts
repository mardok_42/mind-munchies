/**
 * Everything is an object - almost
 *
 * This is important to understand so you know how and why things interact with each other.
 */
export const someObject = { stuff: "junk" }; // Is an object
export const someArray = [someObject]; // Is an object
export const someNull = null; // Is an object
export const someString = "Some String"; // Is a string, but has descended from an object
export const someNumber = 42; // It really is a number
export const someBoolean = true; // It really is a boolean.
