import LibraryToMock, { funWithTimer, SomeClass, someFun } from "./015_library_to_mock";

/**
 * These functions are to show give different obstacles to overcome in testing.
 */

// Can you mock the default of the library?
export function isItTrue(str: string): boolean {
  return LibraryToMock.fun1(str).realFun;
}

// Can you mock a timer? And handle open handlers in tests? You should not need to wait forever.
export async function waitOnIt(time = 1000000) {
  await funWithTimer(time);

  return "that was long";
}

// Can you mock a function in a library?
export function areYouHavingFunTesting() {
  return someFun().stuff === "things";
}

// Can you mock a class in a library
export function isItClassy() {
  const obj = new SomeClass();
  console.log("obj: ", obj);
  return obj.anotherFun();
}
