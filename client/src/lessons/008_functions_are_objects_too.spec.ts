import { myFun1, myFun2, myFun3, myFun4 } from "./008_functions_are_objects_too";

describe("functions are objects too", () => {
  const one = 1;
  const two = 2;
  const three = 3;
  const expected = 6;

  it("builds a function object from Function", () => {
    const result = myFun1(one, two, three); // funName(...args) is actually shorthand for funName.call(this, ...args)

    expect(result).toBe(expected);
    expect(myFun1.name).toBe("anonymous"); // Functions built with "new Function" do not have names
  });

  it("can be called", () => {
    // call passes the scope as the first argument, then the normal arguments. This is actual way it is used. funName() is shorthand.
    const result = myFun1.call(this, one, two, three);

    expect(result).toBe(expected);
  });

  // Using call is handy when binding scope to the function. We will talk about binding with scope.
  it("can be called with arguments", () => {
    const args = [this, one, two, three];

    // arguments is an array. This works.
    // @ts-ignore
    const result = myFun1.call(...args);

    expect(result).toBe(expected);
  });

  it("can be also called like this", () => {
    const args = [one, two, three];

    // This also works
    const result = myFun1(...args);

    expect(result).toBe(expected);
  });

  it("builds a function object from function", () => {
    // When building the traditional way, functions have their own scope and given name
    const result = myFun2(one, two, three);

    expect(result).toBe(expected);
    expect(myFun2.name).toBe("myFun2"); // Functions have names
  });

  it("builds an arrow function", () => {
    // Arrow Functions are almost the same. they have the scope of their parent
    const result = myFun3(one, two, three);

    expect(result).toBe(expected);
    expect(myFun3.name).toBe("myFun3"); // Arrow functions have names too
  });

  it("builds a shorter arrow function", () => {
    const result = myFun4(one, two, three);

    expect(result).toBe(expected);
  });
});
