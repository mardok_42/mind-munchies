import { someArray, someBoolean, someNull, someNumber, someObject, someString } from "./007_everythings_an_objects";

describe("Everything is an object", () => {
  it("is an object as an object", () => {
    expect(typeof someObject).toBe("object");
  });
  it("is an object as an array", () => {
    expect(typeof someArray).toBe("object");

    console.log("keys: ", Object.keys(someArray)); // [ '0' ]
  });
  it("is an object as a null", () => {
    expect(typeof someNull).toBe("object");
  });
  it("is not an object as a string", () => {
    const someString = "Some String"; // Is a string, but has descended from an object
    // Strings are not strictly objects, but they are descended from them. They are sealed, frozen, and are not extensible.
    expect(typeof someString).toBe("string");

    const descriptors = Object.getOwnPropertyDescriptors(someString);
    // Look it has descriptors
    console.log("getOwnPropertyDescriptors: ", descriptors);
    console.log("isSealed: ", Object.isSealed(someString)); // true
    console.log("isFrozen: ", Object.isFrozen(someString)); // true
    console.log("isExtensible: ", Object.isExtensible(someString)); // false

    // @ts-ignore
    // expect(descriptors.length.value).toBe(11);

    // Because a string is an Array, is an Object you can do stuff like this.
    // @ts-ignore
    const [thingy, oo, mm, ee, ...str] = someString;
    console.log("ss: ", thingy); // s
    console.log("oo: ", oo); // o
    console.log("mm: ", mm); // m
    console.log("ee: ", ee); // e
    console.log("str: ", str); // str
  });
  it("is not an object. It is number", () => {
    // Numbers are not objects. They are just numbers.
    expect(typeof someNumber).toBe("number");

    const descriptors = Object.getOwnPropertyDescriptors(someNumber);
    // Numbers have no descriptors
    console.log("getOwnPropertyDescriptors: ", descriptors);
    expect(descriptors).toEqual({});
  });

  it("is not an object. It is a boolean", () => {
    // Boolean are not objects. They are just Boolean.
    expect(typeof someBoolean).toBe("boolean");

    const descriptors = Object.getOwnPropertyDescriptors(someBoolean);
    // Boolean have no descriptors
    console.log("getOwnPropertyDescriptors: ", descriptors);
    expect(descriptors).toEqual({});
  });
});
