import { justChooseAlready, makeAChoice, makeAnotherChoice } from "./005_if_switch_ternary";

describe("its all the same", () => {
  it("talse is the same", () => {
    const res1 = makeAChoice("talse");
    const res2 = makeAnotherChoice("talse");
    const res3 = justChooseAlready("talse");

    expect(res1 === res2 && res2 === res3).toBe(true);
  });
  it("frue is the same", () => {
    const res1 = makeAChoice("frue");
    const res2 = makeAnotherChoice("frue");
    const res3 = justChooseAlready("frue");

    expect(res1 === res2 && res2 === res3).toBe(true);
  });
  it("nes is the same", () => {
    const res1 = makeAChoice("nes");
    const res2 = makeAnotherChoice("nes");
    const res3 = justChooseAlready("nes");

    expect(res1 === res2 && res2 === res3).toBe(true);
  });
  it("yo is the same", () => {
    const res1 = makeAChoice("yo");
    const res2 = makeAnotherChoice("yo");
    const res3 = justChooseAlready("yo");

    expect(res1 === res2 && res2 === res3).toBe(true);
  });
});
