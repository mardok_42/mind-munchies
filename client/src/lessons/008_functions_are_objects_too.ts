/**
 *
 * ### Functions are objects too ###
 *
 * Reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions
 *
 * Functions are Objects too. The are simply objects that are callable.
 *
 * Anatomy of a function - this creates an object that is callable
 *
 * function functionName (arrayOfArguements) {
 *   function body
 * }
 *
 */

// eval is a function that turns strings into javascript. Its dangerous because it can throw pretty easy with syntax errors. But if you ever build a module loader, you will need it.
eval('console.log("Yep this runs")');

// You can build functions with "Function"
// The last argument is the function body, and the proceeding are variable names. The body should be a string that eval will be run on.
export const myFun1 = new Function("one", "two", "three", "return one + two + three");

// This is the same
export function myFun2(one: number, two: number, three: number) {
  return one + two + three;
}

// this is mostly the same - we will talk about scope later
export const myFun3 = (one: number, two: number, three: number) => {
  return one + two + three;
};

// Also mostly the same
export const myFun4 = (one: number, two: number, three: number) => one + two + three;
