interface MyClass2Interface {
  funAdd: (num: number) => number;
  funSub: (num: number) => number;
}

export class MyClass {
  thing1: number;

  constructor() {
    this.thing1 = 0;
  }

  fun1(num: number) {
    console.log("it was called");
    this.thing1 = num + this.thing1;
  }
}

/**
 * When you implement an interface, it will want all the stuff from the interface, but have non of it. It is only a guide on what should be there.
 */
export class MyClass2 implements MyClass2Interface {
  someNum = 0;

  funAdd(num: number): number {
    this.someNum = num + this.someNum;
    return this.someNum;
  }

  funSub(num: number): number {
    this.someNum = this.someNum - num;
    return this.someNum;
  }

  getRandomNumber(min = 0, max = 100): number {
    return Math.random() * (max - min) + min;
  }
}

/**
 * When you extend a class It will have all the stuff from the parent extended class. You can overwrite functions or add to them.
 */
export class MyClass3 extends MyClass {
  thing2 = 0;

  // If a constructor has nothing in it but super, it is not needed.

  funAdd(num: number): number {
    this.thing2 = this.thing2 + this.thing1 + num;
    return this.thing2;
  }
}
