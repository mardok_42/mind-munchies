/**
 * ### Object Scope
 *
 * At what level can variables be accessed? Variables can have the same name, but it is the scope that they are declared in that matters.
 *
 */

export const var1 = 80;

export const SomeObject = {
  var1: 24,
  // A closure is a function that is within the scope of an object.
  myClosure: function () {
    const var1 = 42;

    return var1;
  },
  getRootVar1: function () {
    return var1; // 80;
  },
  getVar1: function () {
    return this.var1; // 24 or what setVar1 sets it to
  },
  setVar1: function (num: number) {
    this.var1 = num;
  }
};

////////////////////////////
////////////////////////////
////////////////////////////

export function SomeFunction() {
  let var1 = 24; // Private variable
  // @ts-ignore
  this.var1 = 13; // Public variable

  // @ts-ignore
  this.myClosure = function () {
    // Functions are objects. This function is within a functions scope.
    const var1 = 42;

    return var1;
  };

  // @ts-ignore
  this.getRootVar1 = function () {
    return var1; // 24 on initialization
  };

  // @ts-ignore
  this.getVar1 = function () {
    return var1;
  };

  // @ts-ignore
  this.setVar1 = function (num: number) {
    var1 = num;
  };

  // @ts-ignore;
  this.willWork = () => {
    // Note: Arrow functions and functions keep the scope of the parent object.
    // @ts-ignore;
    return this.var1;
  };
}

////////////////////////////
////////////////////////////
////////////////////////////

export class SomeClass {
  var1 = 24;

  myClosure() {
    const var1 = 42;

    return var1;
  }

  getRootVar1() {
    return var1; // This is the scope of the page
  }

  getVar1() {
    return this.var1;
  }

  setVar1(num: number) {
    this.var1 = num;
  }

  // Note: Arrow functions and functions keep the scope of the parent object.
  willWork = function () {
    return this.var1;
  };
}

// "this" scopes to the object. The scope remains with the last method declared with "function".
export function badScope() {
  console.log();
  // @ts-ignore
  this.var1 = "not 80";
  function willNotWork() {
    // @ts-ignore
    return this.var1; // Should throw "Cannot read property 'var1' of undefined"
  }
  return willNotWork();
}
