import { getterSetterObject } from "./010_getter_setter";

describe("getter setter", () => {
  it("sets a variable", () => {
    const spy = jest.spyOn(console, "log");

    getterSetterObject.someValue = 24;

    expect(spy).toHaveBeenCalledWith("setter called");
  });

  it("gets a variable", () => {
    const spy = jest.spyOn(console, "log");

    const val = getterSetterObject.someValue;
    console.info(`val: `, val);

    expect(spy).toHaveBeenCalledWith("getter called");
  });

  it("sets a variable too", () => {
    const spy = jest.spyOn(console, "log");

    getterSetterObject.setValue = 42;
    const val = getterSetterObject.setValue; // The value of a setter is undefined

    expect(spy).toHaveBeenCalledWith("setter called too");
    expect(val).toBe(undefined);
  });

  it("gets a variable too", () => {
    const spy = jest.spyOn(console, "log");

    const val = getterSetterObject.getValue;
    console.info(`val: `, val);

    expect(spy).toHaveBeenCalledWith("getter called too");
  });
});
