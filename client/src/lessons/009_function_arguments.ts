/**
 * ### Function Arguments
 *
 * arguments is the array like structure that is past into a function.
 *
 * It has a few special things arrays do not.
 */

export function argumentsLength() {
  return arguments.length; // arguments is a key word. You can use it to access all the things passed into a function.
}

export function argumentsLengthToo(...args: any[]) {
  // You can spread the arguments to get the same thing.
  return args.length;
}

export function thirdArguement() {
  return arguments[2];
}

type Thing = "other stuff" | "more stuff" | "Hellow World" | "junk";
// You can set defaults for your arguments
export function withDefault(stuff: Thing = "junk") {
  return stuff;
}
