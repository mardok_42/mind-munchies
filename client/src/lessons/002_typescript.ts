/**
 *
 * ### Typescript ###
 *
 * What is Typescript?
 * Typescript is a syntactic sugar on Javascript. It does not add anything to the language. It is an aid to development.
 * It only shows you during development if something may be wrong. You can also set your compiler to warn or error out if a rule is broken.
 * You can set the rules. It can be as strict or loose as you want. But the looser it is the less useful it will be.
 * You should find a ballance for the project that allows for safety and speed. If the project if wrought with bugs, maybe turn up the rules.
 * If there are few bugs, but people spend lots of time attempting to figure out typescript warnings, maybe turn down the rules.
 *
 * Why Typescript? It is an aid in scripting javascript because it is a loosely typed language.
 * Javascript on its own will let you get away with no end of bad things. Typescript makes it act like a strongly typed language.
 * This ensures stronger contracts between methods or errors. Which means less bugs.
 */

/**
 * Basic Types
 *
 * Most things are going to be built of these four basic types: undefined, number, string or boolean.
 * These are the most used basic building blocks of Javascript. There are other basic types but they are rarely used.
 * In its essence typescript checks to see if the expected basic type matches the type it is given.
 *
 */
let myNumber: number;
let myString: string;
let myBool: boolean;

myNumber = 1;
// myNumber = false; // Error: Type 'boolean' is not assignable to type 'number'.
console.log("myNumber: ", myNumber);

myString = "Bob";
// myString = 1; // Error: Type 'number' is not assignable to type 'string'.
console.log("myString: ", myString);

myBool = true;
// myBool = 1; // Error: Type 'number' is not assignable to type 'boolean'.
console.log("myBool: ", myBool);

/**
 *
 * undefined and null (Its not there)
 * If you don't want to set the value don't use "| undefined;". Just use "?"
 */
type ImThere = string | number | boolean;
export interface SomeStuff {
  imThere: ImThere;
  iMightBeThere?: ImThere; // If you use this way you do not need to set the value.
  iMightBeThere2: ImThere | undefined; // If done this way you will need to always set the value.
  iMightNotBeThere: ImThere | null;
}

const someStuff: SomeStuff = {
  imThere: "fhjbk",
  iMightBeThere2: undefined, // Property 'iMightBeThere2' is missing in type '{ imThere: true; iMightNotBeThere: null; }' but required in type 'SomeStuff'.ts
  iMightNotBeThere: null
};
console.log("someStuff: ", someStuff);

/**
 *
 * Basic Functions
 *
 * Define the contract. What is going in. What is coming out.
 *
 */
function myFunction(val1: number, val2: number): string {
  return `The answer is ${val1 + val2}`;
}

const myFunctionRes = myFunction(40, 2); // The answer is 42
// const myFunctionRes2 = myFunction('40', 2); // Error: Argument of type 'string' is not assignable to parameter of type 'number'.
console.log("myFunctionRes: ", myFunctionRes);

/**
 *
 * Basic Unions
 *
 * Sometimes something might be more than one type.
 *
 * type ValidError = Error | ServiceError | AxiosError
 *
 * Or
 *
 * type date = Date | number | string;
 *
 */
type MyUnion1 = number | string;
function myFunction2(val1: MyUnion1, val2: number): string {
  return `The answer is ${val1 + String(val2)}`;
}

const myFunction2Res = myFunction2("4", 2); // The answer is 42
const myFunction2Res2 = myFunction2(4, 2); // The answer is 42
// const myFunction2Res2 = myFunction2(true, 2); // Error: Argument of type 'boolean' is not assignable to parameter of type 'MyUnion1'
console.log("myFunction2Res: ", myFunction2Res);
console.log("myFunction2Res2: ", myFunction2Res2);

/**
 *
 * Enums (not just for typescript)
 *
 * Enums are not only usable in typescript, they also can be used in actual code.
 * They are actual JavaScript objects. That means that they exist after compile time. But they also are recognized by typescript.
 *
 */

enum Colors {
  Red, // 0
  Green, // 1
  Blue // 2
}

// Declaring a number out of order will cause the other indexes to change as well.
enum Names {
  Bob = 4, // 4
  Jim, // 5
  Fred, // 6
  Susan // 7
}

function myFunction3(colorVal: Colors, nameVal: Names): number {
  return colorVal + nameVal;
}

const myFun3Res = myFunction3(Colors.Green, Names.Jim); // 6
console.log("myFun3Res: ", myFun3Res);
// const myFun3ResBad = myFunction3(Colors.Yellow, Names.Jim); // Property 'Yellow' does not exist on type 'typeof Colors'.
// const myFun3ResLimitation = myFunction3(2, Names.Jim); // Argument of type '3' is not assignable to parameter of type 'Colors'.
// console.log("myFun3ResLimitation: ", myFun3ResLimitation); // 8

// You can declare strings as the enum keys. This is a bit more structured, but you get a sense of redundancy.
enum BoyNames {
  Bob = "Bob",
  Jim = "Jim",
  Fred = "Fred"
}

enum GirlNames {
  Susan = "Susan",
  Jane = "Jane",
  Roberta = "Roberta"
}

function myFunction4(boy: BoyNames, girl: GirlNames): string {
  return `${boy} + ${girl}`;
}

const myFun4Res = myFunction4(BoyNames.Bob, GirlNames.Susan); // Bob + Susan
// const myFun4ResBad = myFunction4(BoyNames.George, GirlNames.Susan); // Error: Property 'George' does not exist on type 'typeof BoyNames'
console.log("myFun4Res: ", myFun4Res);

enum KeyValues {
  KEY1 = "value1",
  KEY2 = "value2",
  KEY3 = "value3"
}
type Key = keyof typeof KeyValues; // Get the Keys

const obj = {
  [KeyValues.KEY1]: "stuff",
  [KeyValues.KEY2]: "junk",
  [KeyValues.KEY3]: "things"
};

function myFunction5(key: Key): KeyValues {
  return KeyValues[key];
}

// I see this occasionally. You need to ask for the key not the enum value.
// function myFunction5Bad(key: KeyValues) {
//   return KeyValues[key]; // Error: Property '[KeyValues.KEY1]' does not exist on type 'typeof KeyValues'.
// }
const myFun5Res = myFunction5("KEY2"); // value2
console.log("myFun5Res: ", myFun5Res);

type ObjKey = keyof typeof obj;
function myFunction5a(key: ObjKey): string {
  return obj[key];
}

const myFun5aRes = myFunction5a(KeyValues.KEY1); // value2
console.log("myFun5aRes: ", myFun5aRes);

function myFunction6(value: KeyValues): boolean {
  return KeyValues.KEY3 === value;
}
const myFun6Res = myFunction6("value3" as KeyValues);
// const myFun6ResBad = myFunction6("value3"); // Error: Argument of type '"value3"' is not assignable to parameter of type 'KeyValues'.
console.log("myFun6Res: ", myFun6Res);

/**
 *
 * String Literals Unions
 * These work a bit better then enums. They do get destroyed on compile type, but the error messages are clearer.
 */

type MoreBoyNames = "George" | "Larry" | "Albert";
type MoreGirlNames = "Laura" | "Betty" | "Ericka";

function myFunction7(boy: MoreBoyNames, girl: MoreGirlNames): string {
  return `${boy} + ${girl}`;
}

const myFun7Res = myFunction7("Larry", "Ericka"); // Larry + Ericka
// const myFun7ResBad = myFunction7("Larry", "Jane"); // Error: Argument of type '"Jane"' is not assignable to parameter of type 'MoreGirlNames'
console.log("myFun7Res: ", myFun7Res);

/**
 * Arrays and Tuple Types
 *
 */
type MyArray = string[];
type MyTuple = [string]; // I see this often. It is a thing, but not the way I usually see it used.
// Tuples should be used on arrays that have different values for each element. Such as:
interface Data {
  things: string[];
}
type Loading = boolean;
type MyTuple2 = [Data, Error, Loading];

const myArray: MyArray = ["stuff", "Junk", "Things"];
console.log("myArray: ", myArray);

const myTuple: MyTuple = ["stuff"];
console.log("myTuple: ", myTuple);
// const myTupleBad: MyTuple = ["stuff", "Junk", "Things"];
// console.log("myTupleBad: ", myTupleBad);
// Type '[string, string, string]' is not assignable to type 'MyTuple'.
// Source has 3 element(s) but target allows only 1.

/**
 *
 * Generics - Because sometimes you have slightly different stuff
 *
 */

// T is often used as the variable, but it can be any string. Simply put the variables you want to use in the <> after the function name.
function myFunction8<T>(arr: T[]): T[] {
  return arr.sort();
}

const stringArr = ["Susan", "George", "Jill", "Bob"];
const sortedStrings = myFunction8<string>([]); // ["Bob", "George", "Jill", "Susan"]
// const sortedStringsBad = myFunction8<number>(stringArr); // Argument of type 'string[]' is not assignable to parameter of type 'number[]'.
console.log("sortedStrings: ", sortedStrings);

// For interfaces the definition is declared after the interface name. To set a default, use = and a default type.
// So here, the first argument is required, and the second is optional and defaulted to Error.
interface MyResponse<Data, E = Error> {
  data: Data;
  error?: E;
}

interface MyData {
  stuff: string;
  things: string;
}

function myFunction9(): MyResponse<MyData> {
  return {
    data: {
      stuff: "Trains",
      things: "Planes"
    },
    error: new Error("Bad Things")
  };
}
console.log(myFunction9());

interface MyError {
  someMessage: string;
  someCode: number;
}
function myFunction10(): MyResponse<MyData, MyError> {
  return {
    data: {
      stuff: "Trains",
      things: "Planes"
    },
    error: {
      someMessage: "Its mostly harmless",
      someCode: 418
    }
  };
}
console.log(myFunction10());

/**
 *
 * Combining Stuff
 */
type Occupation = "Lamp Lighter" | "Milk Man" | "Fishmonger" | "Swineherd" | "Dairy Maid";

interface DbEntry {
  id: string;
  firstName: string;
  lastName: string;
  occupation: Occupation;
}
const dbEntry: DbEntry = {
  id: "4567",
  firstName: "Bob",
  lastName: "Jones",
  occupation: "Lamp Lighter"
  // occupation: "programer" // Type '"programer"' is not assignable to type 'Occupation'.
};

interface MutationEntry {
  fullName: string;
}
// Interfaces can be combined to fuse the properties of multiple objects.
type Entry = DbEntry & MutationEntry;

function entryFactory(dbEntry: DbEntry): Entry {
  return { ...dbEntry, fullName: `${dbEntry.firstName} ${dbEntry.lastName}` };
}

const entry = entryFactory(dbEntry);
console.log("entry: ", entry);

////////////////////////////// Or /////////////////////////////

interface MutationEntry {
  fullName: string;
}
// Instead of merging two objects. you can extend one object from another. So there is a parent and child object.
interface EntryExtended extends DbEntry {
  fullName: string;
}

function entryFactory2(dbEntry: DbEntry): EntryExtended {
  return { ...dbEntry, fullName: `${dbEntry.firstName} ${dbEntry.lastName}` };
}

const entry2 = entryFactory2(dbEntry);
console.log("entry2: ", entry2);

/**
 *
 * Hacks to get around stuff. Useful for tests. Don't do this in real code.
 *
 * "unknown" is useful for getting around typescript in tests. Make an object, declare it unknown, then declare it as the expected type.
 */
const var1 = { firstName: "Chuck", lastName: "Norris" };
const var2 = var1 as unknown as DbEntry;
// const resBad = entryFactory2(var1); // Argument of type '{ firstName: string; lastName: string; }' is not assignable to parameter of type 'DbEntry'
const res = entryFactory2(var2);
console.log("fullName: ", res.fullName);
