/**
 * ### Variable types
 *
 * const, let, var - Whats the difference, why do we need more than var.
 *
 */

// var has some irregularities with scope. Because of the scope you would expect the log to be "things".
// But because all "var"s are hoisted(Put at the top of the file and re-declared) It re-declares it as "Junk".
export function useVar() {
  var stuff = "things";
  var condition = true;

  if (condition === true) {
    var stuff = "Junk";
  }

  console.log(stuff); // "Junk"
}

// let is block scoped. So the let within the 'condition === true' is different than the let at the top of the function. let can be updated, but not re-declared as var can be.
export function useLet() {
  let stuff = "things";
  const condition = true;

  stuff = "you can do this";
  if (condition === true) {
    /// code here
    let stuff = "Junk"; // 'stuff' is declared but its value is never read.
  }

  console.log(stuff); // "things"
}

export function useConst() {
  const stuff = "things";
  const condition = true;

  if (condition === true) {
    const stuff = "Junk"; // 'stuff' is declared but its value is never read.
  }
  // stuff = 'Can not do' // Cannot assign to 'stuff' because it is a constant.

  console.log(stuff); // "things"
}
