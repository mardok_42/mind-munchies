import { useVar, useLet, useConst } from "./012_variables";

describe("variables", () => {
  afterEach(() => jest.clearAllMocks());

  it("calls useVar", () => {
    const spy = jest.spyOn(console, "log");

    useVar();

    expect(spy).toHaveBeenCalledWith("Junk");
  });

  it("calls useLet", () => {
    const spy = jest.spyOn(console, "log");

    useLet();

    expect(spy).toHaveBeenCalledWith("you can do this");
  });

  it("calls useConst", () => {
    const spy = jest.spyOn(console, "log");

    useConst();

    expect(spy).toHaveBeenCalledWith("things");
  });
});
