/**
 *
 * ### Loops ###
 *
 * Why loop over things?
 * When it comes down to it, programming is all about comparing, modifying, and displaying data. We need various ways to get to that data. Loops are a major way to do so.
 *
 * Referance: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration.
 *
 */

const speedTestArr = Array(10000);

///////////////////////////////////////////////////////////////
//////////////////// for /////////////////////////////////////
/////////////////////////////////////////////////////////////
export function aForLoop() {
  const start = Date.now();

  // For loop
  for (let i = 0; i < speedTestArr.length; i++) {
    // do stuff
    console.log("iterator: ", i);
  }

  const res = Date.now() - start;
  console.log("aForLoop finished: ", res);
  return res;
}

///////////////////////////////////////////////////////////////
//////////////////// while ///////////////////////////////////
/////////////////////////////////////////////////////////////
// * Dangerous, because its easy to make it run forever
export function aWhileLoop() {
  const start = Date.now();
  const a = 2;
  let b = 2;
  let i = 0;

  // While loop
  while (a === b) {
    // do stuff
    console.log("iterator: ", i);
    if (i > 10) {
      b = 3;
    }
    i++;
  }

  const res = Date.now() - start;
  console.log("aWhileLoop finished: ", res);
  return res;
}

///////////////////////////////////////////////////////////////
//////////////////// do while ////////////////////////////////
/////////////////////////////////////////////////////////////
// * also dangerous, and will always run at least once
export function aDoWhileLoop() {
  const start = Date.now();
  const a = 2;
  const b = 2;
  let i = 0;

  // do while
  do {
    // do stuff
    console.log("iterator: ", i);
    i++;
  } while (a !== b);

  const res = Date.now() - start;
  console.log("aDoWhileLoop finished: ", res);
  return res;
}

///////////////////////////////////////////////////////////////
//////////////////// for in //////////////////////////////////
/////////////////////////////////////////////////////////////
// variable in object - Property names
export function aForInLoop() {
  const start = Date.now();
  const obj = { foo: "bar", stuff: "things" };

  // for in loop
  for (let itemName in obj) {
    // @ts-ignore
    console.log(`${itemName}: ${obj[itemName]}`);
  }
  const res = Date.now() - start;
  console.log("aForInLoop finished: ", res);
  return res;
}

///////////////////////////////////////////////////////////////
//////////////////// for of //////////////////////////////////
/////////////////////////////////////////////////////////////
// - Property values. Only for iterable objects like arrays
export function aForOfLoop() {
  const start = Date.now();
  const arr = ["a", "b", "c"];

  // for of loop
  for (let item of arr) {
    // @ts-ignore
    console.log(`item: `, item);
  }
  const res = Date.now() - start;
  console.log("aForOfLoop finished: ", res);
  return res;
}

///////////////////////////////////////////////////////////////
//////////////////// label break continue ////////////////////
/////////////////////////////////////////////////////////////
export function labelTest(type?: "break" | "continue") {
  loop1: for (let i = 0; i < 10; i++) {
    console.log("loop1 start: ", i);
    loop2: for (let j = 0; j < 10; j++) {
      console.log("Loop2 start: ", j);
      if (type === "break") {
        break loop1; // break will stop execution of the loop1 loop
      } else if (type === "continue") {
        continue loop1; // continue will send to the loop1 start
      }
      console.log("loop2 end: ", j);
    }
    console.log("loop1 end: ", i);
  }
}

///////////////////////////////////////////////////////////////
//////////////////// Loop Speed Test /////////////////////////
/////////////////////////////////////////////////////////////

// This used to be slowest, but does not appear to be anymore. Using less memory appears to be faster than the length evaluation.
export function forLoopEveal() {
  const start = Date.now();
  for (let i = 0; i < speedTestArr.length; i++) {
    // do stuff
    console.log("iterator: ", i);
  }

  const res = Date.now() - start;
  console.log("forLoopPlus finished: ", res);
  return res;
}

// Slowest loop
export function forLoopPlus() {
  const start = Date.now();
  const len = speedTestArr.length;
  console.log("len: ", len);
  for (let i = 0; i < len; i++) {
    // do stuff
    console.log("iterator: ", i);
  }

  const res = Date.now() - start;
  console.log("forLoopPlus finished: ", res);
  return res;
}

// Going backwards goes a bit faster, but only a very little bit. In small sets it might will be very close to or greater than forwards. You need sets of > 1000000 to start seeing the difference.
export function forLoopMinus() {
  const start = Date.now();
  const len = speedTestArr.length;
  console.log("len: ", len);
  for (let i = len; i > 0; i--) {
    // do stuff
    console.log("iterator: ", i);
  }

  const res = Date.now() - start;
  console.log("forLoopMinus finished: ", res);
  return res;
}

// This is the fastest. Less memory usage.
export function whileMinus() {
  const start = Date.now();
  let num = speedTestArr.length;
  while (num--) {
    console.log("iterator: ", num);
  }

  const res = Date.now() - start;
  console.log("whileMinus finished: ", res);
  return res;
}
