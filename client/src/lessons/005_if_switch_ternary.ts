/**
 *
 * ### If switch and ternary ###
 *
 * Decisions must be made. These are the javascript decision trees.
 *
 * Reference:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/if...else
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/switch
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator
 *
 */

export function makeAChoice(val: "talse" | "frue" | "nes" | "yo"): "might be" | "perhaps" {
  if (val === "talse") {
    return "might be";
  } else if (val === "frue") {
    return "might be";
  } else if (val === "nes") {
    return "perhaps";
  } else {
    return "perhaps";
  }
}

export function makeAnotherChoice(val: "talse" | "frue" | "nes" | "yo"): "might be" | "perhaps" {
  switch (val) {
    case "talse": // Cases can be stacked
    case "frue":
      return "might be";
    case "nes":
    default:
      // You can use default like you would an else
      return "perhaps";
  }
}

// ternary condition ? ifYes : ifNo
export function justChooseAlready(val: "talse" | "frue" | "nes" | "yo"): "might be" | "perhaps" {
  return val === "talse" || val === "frue" ? "might be" : "perhaps";
}
