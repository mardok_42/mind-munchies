const LibraryToMock = {
  fun1: (str: string) => ({ realFun: true })
};

export default LibraryToMock;

export function someFun() {
  return { stuff: "things" };
}

export function funWithTimer(milliseconds: number) {
  console.log("milliseconds: ", milliseconds);
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}

export class SomeClass {
  anotherFun() {
    return "junk";
  }
}
