/**
 *
 * ### Object Descriptors ###
 * Deep Down - Just about everything in Javascript is an object. Here is some odd stuff that can be done with them.
 *
 * Reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object
 *
 * Today we are going to look at immutable objects and what that means.
 *
 * immutable: unchanging over time or unable to be changed.
 *
 * Sealed and Frozen objects are Immutable. Sealed makes the structure unchangeable. Frozen makes the values immutable as well. Both sealed and frozen are shallow. You can stuff change the child objects.
 */

/**
 * Sealed Objects have an immutable structure. You can still change the values.
 */
export function sealedTest() {
  const obj = { stuff: "junk", childObj: {} };

  console.log("descriptors on init: ", Object.getOwnPropertyDescriptors(obj));

  Object.seal(obj);
  console.log("descriptors after seal: ", Object.getOwnPropertyDescriptors(obj));
  /*
   stuff: {
      value: 'junk',
      writable: true,
      enumerable: true,
      configurable: false
    },
  */

  console.log("isSealed: ", Object.isSealed(obj)); // true
  console.log("isFrozen: ", Object.isFrozen(obj)); // false
  console.log("isExtensible: ", Object.isExtensible(obj)); // false

  obj.stuff = "things"; // is writable
  // delete obj.stuff; // TypeError: Cannot delete property 'stuff' of #<Object>
  // obj.moreStuff = "turtles"; // TypeError: Cannot add property moreStuff, object is not extensible
  // @ts-ignore
  obj.childObj.iCanDoThis = true; // Sealing an obj makes a shallow immutable object. child objects can still be changed
}

/**
 * Frozen Objects have an immutable structure, and immutable values.
 */
export function frozenTest() {
  const obj = { stuff: "junk", childObj: {} };

  console.log("descriptors on init: ", Object.getOwnPropertyDescriptors(obj));

  Object.freeze(obj);
  console.log("descriptors after freeze: ", Object.getOwnPropertyDescriptors(obj));
  /*
    stuff: {
      value: 'junk',
      writable: false,
      enumerable: true,
      configurable: false
    },
  */

  console.log("isSealed: ", Object.isSealed(obj)); // true
  console.log("isFrozen: ", Object.isFrozen(obj)); // true
  console.log("isExtensible: ", Object.isExtensible(obj)); // false

  // obj.stuff = "things"; // TypeError: Cannot assign to read only property 'stuff' of object '#<Object>'
  // delete obj.stuff; // TypeError: Cannot delete property 'stuff' of #<Object>
  // obj.moreStuff = "turtles"; // TypeError: Cannot add property moreStuff, object is not extensible
  // @ts-ignore
  obj.childObj.iCanDoThis = true; // Freezing an obj makes a shallow immutable object. child objects can still be changed
}
