/****
 * ### Memory Management with the Flyweight pattern
 *
 * In our code we often see multiple mutations data as it progresses from its source to the client. We are going to be looking at why this is bad.
 */

interface Stuff {
  id: number;
  name: string;
  data: {
    stuff: string;
  };
}

// Function that rebuilds the data new every time.
export function buildStuff(howMany: number): Stuff[] {
  const arr: Stuff[] = [];

  for (let i = 0; i < howMany; i++) {
    arr.push({
      id: i,
      name: randomString(50),
      data: {
        stuff: randomString(10000)
      }
    });
  }

  return arr;
}

// Function that reuses data with the flyweight pattern
export function buildStuffWithFlyWeight(howMany: number): Stuff[] {
  const arr: Stuff[] = [];

  for (let i = 0; i < howMany; i++) {
    arr.push({
      id: i,
      name: getFlyWeight("name"),
      data: {
        stuff: getFlyWeight("stuff")
      }
    });
  }

  return arr;
}

const flyWeights: Record<string, string> = {};
function getFlyWeight(name: string) {
  if (flyWeights[name]) {
    return flyWeights[name];
  }
  const value = name === "name" ? randomString(50) : randomString(10000);
  flyWeights[name] = value;

  return value;
}

function randomString(length: number) {
  let result = "";
  const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const len = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * len));
  }
  return result;
}
