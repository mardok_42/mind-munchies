import { forLoopEveal, forLoopMinus, forLoopPlus, labelTest, whileMinus } from "./004_loops";

describe("loops", () => {
  it("speed test", () => {
    const testSet = 10;
    const forLoopEvealArr = [];
    const forLoopPlusArr = [];
    const forLoopMinusArr = [];
    const whileMinusArr = [];

    for (let i = 0; i < testSet; i++) {
      forLoopEvealArr.push(forLoopEveal());
      forLoopPlusArr.push(forLoopPlus());
      forLoopMinusArr.push(forLoopMinus());
      whileMinusArr.push(whileMinus());
    }

    const forLoopEvealAvg = forLoopEvealArr.reduce((rtn, num) => rtn + num, 0) / testSet;
    const forLoopPlusArrAvg = forLoopPlusArr.reduce((rtn, num) => rtn + num, 0) / testSet;
    const forLoopMinusAvg = forLoopMinusArr.reduce((rtn, num) => rtn + num, 0) / testSet;
    const whileMinusAvg = whileMinusArr.reduce((rtn, num) => rtn + num, 0) / testSet;

    // Notes are for test with testSet = 20
    console.log("forLoopEvealAvg: ", forLoopEvealAvg); // forLoopEvealAvg:  4797.4
    console.log("forLoopPlusArrAvg: ", forLoopPlusArrAvg); // forLoopPlusArrAvg:  4755.6
    console.log("forLoopMinusAvg: ", forLoopMinusAvg); // forLoopMinusAvg:  4768.45
    console.log("whileMinusAvg: ", whileMinusAvg); // whileMinusAvg:  4715.2
  });

  it("runs labelTest", () => {
    console.log("labelTest");
    labelTest();

    // console.log("labelTest break");
    // labelTest("break");

    // console.log("labelTest continue");
    // labelTest("continue");
  });
});
