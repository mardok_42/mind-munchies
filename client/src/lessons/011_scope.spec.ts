import { var1, SomeObject, SomeFunction, SomeClass, badScope } from "./011_scope";

describe("scope", () => {
  describe("SomeObject", () => {
    it("returns myClosure result", () => {
      const expected = 42;

      const res = SomeObject.myClosure(); // 42

      expect(res).toBe(expected);
    });

    it("returns getRootVar1 result", () => {
      const expected = 80;

      const res = SomeObject.getRootVar1(); // 80

      expect(res).toBe(expected);
    });

    it("returns getVar1 result", () => {
      const expected = 24;

      const res = SomeObject.getVar1(); // 24

      expect(res).toBe(expected);
    });

    it("returns getVar1 result again", () => {
      const expected = 57;
      SomeObject.setVar1(expected);

      const res = SomeObject.getVar1(); // 24

      expect(res).toBe(expected);
    });
  });

  describe("SomeFunction", () => {
    // @ts-ignore
    const someObject = new SomeFunction();

    it("returns myClosure result", () => {
      const expected = 42;

      const res = someObject.myClosure(); // 42

      expect(res).toBe(expected);
    });

    it("returns getRootVar1 result", () => {
      // @ts-ignore
      const someObject = new SomeFunction();
      const expected = 24;

      const res = someObject.getRootVar1(); // 24

      expect(res).toBe(expected);
    });

    it("returns getVar1 result", () => {
      const expected = 24;

      const res = someObject.getVar1(); // 24

      expect(res).toBe(expected);
    });

    it("returns getVar1 result again", () => {
      const expected = 57;
      someObject.setVar1(expected);

      const res = someObject.getVar1(); // 57

      expect(res).toBe(expected);
    });
  });

  describe("SomeClass", () => {
    // @ts-ignore
    const someObject = new SomeClass();

    it("returns myClosure result", () => {
      const expected = 42;

      const res = someObject.myClosure(); // 42

      expect(res).toBe(expected);
    });

    it("returns getRootVar1 result", () => {
      // @ts-ignore
      const someObject = new SomeClass();
      const expected = 80;

      const res = someObject.getRootVar1(); // 80

      expect(res).toBe(expected);
    });

    it("returns getVar1 result", () => {
      const expected = 24;

      const res = someObject.getVar1(); // 24

      expect(res).toBe(expected);
    });

    it("returns getVar1 result again", () => {
      const expected = 57;
      someObject.setVar1(expected);

      const res = someObject.getVar1(); // 57

      expect(res).toBe(expected);
    });
  });

  describe("badScope", () => {
    it("return undefined", () => {
      try {
        // @ts-ignore
        new badScope();
        throw new Error("This should throw");
      } catch (err) {
        expect(err.message).toBe(`Cannot read property 'var1' of undefined`);
      }
    });
  });
});
