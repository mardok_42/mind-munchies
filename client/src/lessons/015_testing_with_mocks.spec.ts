import { areYouHavingFunTesting, isItClassy, isItTrue, waitOnIt } from "./015_code_using_lib";
import * as LibToMock from "./015_library_to_mock";

jest.mock("./015_library_to_mock", () => {
  return {
    SomeClass: function () {
      // @ts-ignore
      this.anotherFun = () => "stuff";
    }
  }; // Library - mocked
});

describe("mocking it", () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => jest.clearAllMocks());
  // Can you mock the default of the library? - isItTrue
  it("can mock the default", () => {
    const spy = jest.spyOn(LibToMock.default, "fun1").mockReturnValue({ realFun: false });

    const res = isItTrue("stuff");

    expect(spy).toHaveBeenLastCalledWith("stuff");
    expect(res).toBe(false);
  });

  // Can you mock a timer? And handle open handlers in tests? You should not need to wait forever. - waitOnIt
  /*
    - can we stop:  
      thrown: "Exceeded timeout of 5000 ms for a test.
      Use jest.setTimeout(newTimeout) to increase the timeout value, if this is a long-running test."
  */
  it("can mock a timer", async () => {
    const promise = waitOnIt();

    jest.runAllTimers(); // You can fast forward timers

    const res = await promise;

    expect(res).toBe("that was long");
  });

  // Can you mock a function in a library? areYouHavingFunTesting
  it("can mock a function", () => {
    jest.spyOn(LibToMock, "someFun").mockReturnValue({ stuff: "junk" });

    const res = areYouHavingFunTesting();

    expect(res).toBe(false);
  });

  // Can you mock a class in a library - isItClassy
  // Note this destroys the library and replaces it. This can often interfear with  tests using different patterns. It is best to separate out these tests to other files if the conflict.
  it("can mock a class", () => {
    const res = isItClassy();

    expect(res).toBe("stuff");
  });
});
