// https://stackoverflow.com/questions/12023359/what-do-the-return-values-of-node-js-process-memoryusage-stand-for
import { buildStuff, buildStuffWithFlyWeight } from "./017_Flyweight_pattern";
import numeral from "numeral";

interface Mem {
  rss: number;
  heapTotal: number;
  heapUsed: number;
  external: number;
  arrayBuffers: number;
}

function MemDiff(mem1: Mem, mem2: Mem) {
  console.log("rss diff: ", numeral(mem2.rss - mem1.rss).format("0.0 ib"));
  // console.log("heapTotal diff: ", mem2.heapTotal - mem1.heapTotal);
  // console.log("heapUsed diff: ", mem2.heapUsed - mem1.heapUsed);
  // console.log("external diff: ", mem2.external - mem1.external);
  // console.log("arrayBuffers diff: ", mem2.arrayBuffers - mem1.arrayBuffers);
}

describe("Flyweight pattern", () => {
  it("generates a raw payload", () => {
    const startMem = process.memoryUsage();
    console.log("Normal startMem: ", numeral(startMem.rss).format("0.0 ib"));

    const arr = buildStuff(10000);

    console.log("arr.length: ", arr.length);

    const endMem = process.memoryUsage();
    console.log("Normal endMem: ", numeral(endMem.rss).format("0.0 ib"));

    MemDiff(startMem, endMem);
  });

  it.only("generates a flyWeight payload", () => {
    const startMem = process.memoryUsage();
    console.log("Flyweight startMem: ", numeral(startMem.rss).format("0.0 ib"));

    const arr = buildStuffWithFlyWeight(10000);

    console.log("arr.length: ", arr.length);

    const endMem = process.memoryUsage();
    console.log("Flyweight endMem: ", numeral(endMem.rss).format("0.0 ib"));

    MemDiff(startMem, endMem);
  });
});
