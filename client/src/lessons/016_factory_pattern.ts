/**
 *
 * ### Creational Patterns ###
 * We deal with objects all the time. Here are the 5 standard creation patterns as implemented with javascript.
 *
 * - Factory
 * - Builder
 * - Abstract Factory
 * - Singleton
 * - Prototype
 */

type Color = "blue" | "red" | "green";
interface Props {
  color: Color;
  height: number;
  width: number;
  depth: number;
}
interface Box {
  color: Color;
  y: number;
  x: number;
  z: number;
}

/**
 *
 * ### Factory Pattern ###
 * The Factory Method design
 *
 * A factory transforms one set of data to an object. It will always return the same type of object. It is the most simple type of Creational pattern.
 *
 * We use these all the time with DTO's and Model's to mutate between data sets.
 */
const myOpts: Props = { color: "blue", height: 10, width: 5, depth: 7 };
// Note: Old way - This is how we used to do it back before classes. Typescript does not like it. This is what your code will look like after you transpile it.
function MyFactory(opts: Props) {
  const { color, height, width, depth } = opts;
  // @ts-ignore
  this.color = color;
  // @ts-ignore
  this.y = height;
  // @ts-ignore
  this.x = width;
  // @ts-ignore
  this.z = depth;
}
// @ts-ignore
const box1 = new MyFactory(myOpts);
console.info("box1: ", box1);

// -----------------------------------------------------------------------

// By its nature a class is a factory that will return the same type of object each time it is called.
class BoxFactory implements Box {
  color: Color;
  y: number;
  x: number;
  z: number;

  constructor(opts: Props) {
    const { color, height, width, depth } = opts;
    this.color = color;
    this.y = height;
    this.x = width;
    this.z = depth;
  }
}

//////////////////////////////////////////////////////////////////////////
/**
 *
 * ### Builder Pattern ###
 * The Builder design pattern
 *
 * A builder method uses one or more factory patterns to assemble an object;
 *
 * We use these for DTO and Model building as well.
 *
 */
function blueBoxBuilder(height: number, width: number, depth: number): Box {
  return new BoxFactory({ height, width, depth, color: "blue" });
}

function redBoxBuilder(height: number, width: number, depth: number): Box {
  return new BoxFactory({ height, width, depth, color: "red" });
}

function greenBoxBuilder(height: number, width: number, depth: number): Box {
  return new BoxFactory({ height, width, depth, color: "green" });
}

//////////////////////////////////////////////////////////////////////////
/**
 *
 * ### Abstract Factory Pattern ###
 * The Abstract Factory is often used to do something similar or to return a similar type of data set.
 *
 * Our Dispatcher Service is an abstract factory
 *
 */
function getBox(color: Color): Box {
  const height = 10,
    width = 5,
    depth = 7;
  switch (color) {
    case "blue":
      return blueBoxBuilder(height, width, depth);
    case "green":
      return greenBoxBuilder(height, width, depth);
    case "red":
      return redBoxBuilder(height, width, depth);
  }
}

//////////////////////////////////////////////////////////////////////////
/**
 *
 * ### Singleton Pattern ###
 * There can only be one of this thing. really good for memory intensive stuff.
 *
 */
let myBlueBox: Box;
function singletonBlueBox(): Box {
  if (!myBlueBox) {
    myBlueBox = getBox("blue");
  }
  return myBlueBox;
}
const myBox = singletonBlueBox();
const myOtherBlueBox = singletonBlueBox();

const itsTheSame = myBox === myOtherBlueBox; // true
console.log("itsTheSame: ", itsTheSame);

//////////////////////////////////////////////////////////////////////////
/**
 *
 * ### Prototype Pattern ###
 * Its just a copy. It has the same properties, but the memory location is different
 *
 */
const myOtherBox = Object.assign({}, myBox);

const itsJustNotTheSame = myBox !== myOtherBox; // true
console.log("itsJustNotTheSame: ", itsJustNotTheSame);
