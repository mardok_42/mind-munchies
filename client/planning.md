- Simple Types - 001
- Typescript - 002
- Testing Basics - 003
- Loops - 004
- If, Switch and ternary - 005
- Objects
  - Everythings an object - 006
  - Descriptors - 007
- functions/closure/method

  - functions are objects that can be called - 008

    - new Function (arg1, arg2, ... argN, functionBody)
    - Arrow functions
    - call
    - name

  - arguments - 009

    - the rest ...
    - default param

  - getter / setter - 010

  - Scope - Closures - 011
    - var, const, let, bind and call

- prototype

- Class

  - public
  - private
  - static

  - extends vrs implements

- Class Decorators

- Arrays
  - navigation
  - mutation
- Data Contracts
- Jest
  - Spies
  - Mocks
- Callbacks

- Promises
- Modules
- Jest Mocks
- Patterns
  - Observer or Observable
  - Singleton
  - Factory
  - Builder
    - What is the difference from a Factory?
  - Decorator
  - State Machine
  - Semaphore
  - MVC
- JSON
- XML
- HTML
- JSX
- Express
- XHR
  - Ajax
  - Fetch
  - Axios
- REST
- SQL (This will probably need several sessions that I have not broke up right now)
- NOSQL (This will probably need several sessions that I have not broke up right now)

Wed - Loops
Thurs - If, Switch and ternary
Fri - Objects

####### PROGRESS #######

001 - Simple Types - DONE
002 - Typescript - DONE
003 - Testing Basics - DONE
004 - Loops - DONE
005 - If, Switch and ternary - DONE

- Objects
  006 - Everythings an object - DONE
  007 - Descriptors - DONE
  008 - functions/closure/method - DONE
  009 - arguments - DONE
  010 - getter / setter - DONE
  011 - Scope - Closures - DONE
  012 - var, const, let - DONE
- prototype
- Class
  013 - public
  013 - private
  013 - static
  014 - extends vrs implements
- Class Decorators
- Arrays
  - navigation
  - mutation
- Data Contracts
- Jest
  - Spies
  - Mocks
- Callbacks
- Promises
- Modules
- Jest Mocks
- Patterns
  - Observer or Observable
  - Singleton
  - Factory
  - Builder
    - What is the difference from a Factory?
  - Decorator
  - State Machine
  - Semaphore
  - MVC
- JSON
- XML
- HTML
- JSX
- Express
- XHR
  - Ajax
  - Fetch
  - Axios
- REST
- SQL (This will probably need several sessions that I have not broke up right now)
- NOSQL (This will probably need several sessions that I have not broke up right now)
