/**
 * ### Classes
 *
 * Class Factories
 *
 * Classes are objects that build other objects. They are factory patterns that are a function that builds an object.
 * Public Private and static
 *
 * Referances: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/Private_class_fields
 *
 */

export class MyFactory {
  // constructors are called on object instantiation.
  constructor() {
    console.log("I only happen on build");
  }

  // public is the default. you dont need to declare it.
  myPublicMethod() {
    console.log("You can see me.");
  }

  // you can declare it public if you want to.
  public myOtherPublicMethod() {
    console.log("You can see me too.");
  }

  // This still can be called, but typescript will ignore it and send errors
  private mySemiPrivateMethod() {
    // private methods can only be used by the class
    console.log("I am hiding but am still there");
  }

  #myPrivateMethod() {
    console.log("nobody can see me");
  }

  static myStaticMethod() {
    // static methods can only be used by the class object, not an instantiated object.
    console.log("Im an outsider");
  }

  callThePrivate() {
    this.#myPrivateMethod();
  }
}

// This is the old way. Classes are just functions, and functions are just objects. This will build the same shape of object as above. They are just factories.
export function MyOldFactory() {
  // constructors are called on object instantiation.
  console.log("I only happen on build");

  // public is the default. you dont need to declare it.
  // @ts-ignore
  this.myPublicMethod = () => {
    console.log("You can see me.");
  };

  // you can declare it public if you want to.
  // @ts-ignore
  this.myOtherPublicMethod = () => {
    console.log("You can see me too.");
  };

  const myPrivateMethod = () => {
    // private methods can only be used by the class
    console.log("nobody can see me");
  };

  // @ts-ignore
  this.callThePrivate = () => {
    myPrivateMethod();
  };
}

MyOldFactory.myStaticMethod = () => {
  // static methods can only be used by the class object, not an instantiated object.
  console.log("Im an outsider");
};
