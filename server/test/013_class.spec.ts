import { MyFactory, MyOldFactory } from "./013_class";
// @ts-ignore
import { SpyInstance } from "@types/jest";

describe("class", () => {
  let spy: jest.SpyInstance;
  beforeEach(() => {
    spy = jest.spyOn(console, "log");
  });
  afterEach(() => jest.clearAllMocks());

  describe("MyFactory", () => {
    it("builds", () => {
      const myObj = new MyFactory();

      expect(myObj).toBeDefined();
      expect(spy).toHaveBeenCalledWith("I only happen on build");
    });

    it("calls public", () => {
      const myObj = new MyFactory();

      myObj.myPublicMethod();

      expect(spy).toHaveBeenCalledWith("You can see me.");
    });

    it("calls other public", () => {
      const myObj = new MyFactory();

      myObj.myOtherPublicMethod();

      expect(spy).toHaveBeenCalledWith("You can see me too.");
    });

    it("can call private, but does not want to", () => {
      const myObj = new MyFactory();

      // @ts-ignore
      myObj.mySemiPrivateMethod(); // Property 'mySemiPrivateMethod' is private and only accessible within class 'MyFactory'.

      expect(spy).toHaveBeenCalledWith("I am hiding but am still there");
    });

    // it("cant call private", () => {
    //   const myObj = new MyFactory();

    //   myObj.#myPrivateMethod(); // SyntaxError: Unexpected token '.'
    // });

    it("calls real private", () => {
      const myObj = new MyFactory();

      myObj.callThePrivate();

      expect(spy).toHaveBeenCalledWith("nobody can see me");
    });

    it("calls static", () => {
      MyFactory.myStaticMethod();

      expect(spy).toHaveBeenCalledWith("Im an outsider");
    });
  });

  describe("MyOldFactory", () => {
    it("builds", () => {
      // @ts-ignore
      const myObj = new MyOldFactory();

      expect(myObj).toBeDefined();
      expect(spy).toHaveBeenCalledWith("I only happen on build");
    });

    it("calls public", () => {
      // @ts-ignore
      const myObj = new MyOldFactory();

      myObj.myPublicMethod();

      expect(spy).toHaveBeenCalledWith("You can see me.");
    });

    it("calls other public", () => {
      // @ts-ignore
      const myObj = new MyOldFactory();

      myObj.myOtherPublicMethod();

      expect(spy).toHaveBeenCalledWith("You can see me too.");
    });

    // It is truly private here
    // it("can call private, but does not want to", () => {
    //   // @ts-ignore
    //   const myObj = new MyOldFactory();

    //   // @ts-ignore
    //   myObj.mySemiPrivateMethod(); // Property 'mySemiPrivateMethod' is private and only accessible within class 'MyOldFactory'.

    //   expect(spy).toHaveBeenCalledWith("I am hiding but am still there");
    // });

    // it("cant call private", () => {
    //   const myObj = new MyFactory();

    //   myObj.#myPrivateMethod(); // SyntaxError: Unexpected token '.'
    // });

    it("calls real private", () => {
      // @ts-ignore
      const myObj = new MyOldFactory();

      myObj.callThePrivate();

      expect(spy).toHaveBeenCalledWith("nobody can see me");
    });

    it("calls static", () => {
      // @ts-ignore
      MyOldFactory.myStaticMethod();

      expect(spy).toHaveBeenCalledWith("Im an outsider");
    });
  });
});
